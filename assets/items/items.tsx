<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="items" tilewidth="34" tileheight="34" tilecount="420" columns="14">
 <image source="34x34items.png" width="476" height="1020"/>
 <tile id="136" type="weapon">
  <properties>
   <property name="name" value="Candy Cane"/>
  </properties>
 </tile>
</tileset>
