<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="town1" tilewidth="16" tileheight="16" tilecount="480" columns="30">
 <image source="town1.gif" width="480" height="256"/>
 <tile id="412">
  <properties>
   <property name="collide" type="bool" value="true"/>
   <property name="entrance" value="cave"/>
  </properties>
 </tile>
</tileset>
