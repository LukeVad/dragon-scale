var IndoorsScene = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize: function IndoorsScene() {
    Phaser.Scene.call(this, { key: 'IndoorsScene' });
  },
  preload: function() {},
  init: function(data) {
    this.playerData = this.registry.list.playerData;

    //Set initial location data
    this.currentLocation = this.registry.list.locationData;
    this.indoorID = data.indoorID;
    this.player = data.player;
    //set message
    this.shopObj = this.currentLocation.find(x => x.id === this.indoorID);
  },
  create: function() {
    // change the background to green
    this.cameras.main.setBackgroundColor('rgba(0, 200, 0, 0.5)');
    this.startIndoors();
    // on wake event we call indoors
    this.sys.events.on('wake', this.startIndoors, this);
  },
  startIndoors: function() {
    this.scene.start('UIScene', { shopObj: this.shopObj });
  }
});

// User Interface scene
var UIScene = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize: function UIScene() {
    Phaser.Scene.call(this, { key: 'UIScene' });
  },
  init: function(data) {
    this.shopObj = data.shopObj;
    this.playerData = this.registry.list.playerData;
  },
  create: function() {
    // draw some background for the menu
    this.graphics = this.add.graphics();
    this.graphics.lineStyle(1, 0xffffff);
    this.graphics.fillStyle(0x031f4c, 1);
    this.graphics.strokeRect(2, 170, 90, 100);
    this.graphics.fillRect(2, 170, 90, 100);

    // basic container to hold all menus
    this.menus = this.add.container();

    this.actionsMenu = new ActionsMenu(10, 175, this);

    // the currently selected menu
    this.currentMenu = this.actionsMenu;

    // add menus to the container
    this.menus.add(this.actionsMenu);

    this.indoorScene = this.scene.get('IndoorsScene');

    // when the scene receives wake event
    this.sys.events.on('wake', this.createShop, this);

    this.createShop();
  },
  shopItems: function() {
    this.graphics.strokeRect(1, 110, 318, 50);
    this.graphics.fillRect(1, 110, 318, 50);

    //Get shop items
    this.shopObj.items.map((item, i) => {
      new Item(this, i * 50 + 130, 135, 34, 34, 'item', [item.id])
        .setInteractive()
        .on('pointerdown', () => {
          this.playerData.armour.push({ item });
          if (item.type === 'special') {
            this.shopObj.completed = true;
          }
        });
    });
  },
  createShop: function() {
    var shopMessageStyle = {
      fontSize: '14px',
      fill: '#fff',
      wordWrap: { width: 300, useAdvancedWrap: true }
    };

    this.shopName = this.add.text(16, 16, this.shopObj.name, { fontSize: '12px', fill: '#fff' });
    if (this.shopObj.completed) {
    } else {
      this.shopMessage = this.add.text(16, 36, this.shopObj.message, shopMessageStyle);
      this.shopItems();
    }
  }
});

var MenuItem = new Phaser.Class({
  Extends: Phaser.GameObjects.Text,

  initialize: function MenuItem(x, y, text, scene) {
    Phaser.GameObjects.Text.call(this, scene, x, y, text, {
      color: '#ffffff',
      align: 'left',
      fontSize: 15
    });
  },

  select: function() {
    this.setColor('#f8ff38');
  },

  deselect: function() {
    this.setColor('#ffffff');
  }
});

var Menu = new Phaser.Class({
  Extends: Phaser.GameObjects.Container,

  initialize: function Menu(x, y, scene) {
    Phaser.GameObjects.Container.call(this, scene, x, y);
    this.menuItems = [];
    this.menuItemIndex = 0;
    this.x = x;
    this.y = y;
    this.selected = false;
  },
  addMenuItem: function(unit) {
    var menuItem = new MenuItem(0, this.menuItems.length * 20, unit, this.scene);
    this.menuItems.push(menuItem);
    this.add(menuItem);
    return menuItem;
  }
});

var ActionsMenu = new Phaser.Class({
  Extends: Menu,

  initialize: function ActionsMenu(x, y, scene) {
    Menu.call(this, x, y, scene);

    this.addMenuItem('Exit')
      .setInteractive()
      .on('pointerdown', () => {
        //moves player out of door
        //this.player.y += 10;
        this.scene.scene.wake('WorldScene');
        this.scene.scene.sleep('UIScene');
        this.scene.scene.stop('IndoorsScene');
      });
  }
});
