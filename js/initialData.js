var playerStatsInitial = [
  {
    name: 'luke',
    dragoonLevel: 0,
    items: [],
    armour: [{ id: 136, type: 'weapon', name: 'Candy Cane' }],
    stats: [{ hp: 100, def: 1, atk: 1 }]
  }
];
var dragonStats = [];

var indoorDataInitial = [
  {
    id: 4,
    name: 'cave',
    type: 'special',
    message:
      "It is dangerous out there, you will need something to defend yourself. I know it's not much but please take one of these.",
    completed: false,
    items: [{ id: 136 }, { id: 70 }]
  },
  {
    id: 5,
    name: 'Secret 1',
    type: 'special',
    message: 'yyy'
  }
];
