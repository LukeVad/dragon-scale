var BootScene = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize: function BootScene() {
    Phaser.Scene.call(this, { key: 'BootScene' });

    //Set initial data
    this.playerStats = playerStatsInitial[0];
    this.locations = indoorDataInitial;
  },

  preload: function() {
    // map tiles
    this.load.image('tiles', 'assets/map/town1.gif');

    // map in json format
    this.load.tilemapTiledJSON('map', 'assets/map/town1.json');

    // our two characters
    this.load.spritesheet('player', 'assets/RPG_assets.png', { frameWidth: 16, frameHeight: 16 });

    //items
    this.load.spritesheet('item', 'assets/items/34x34items.png', {
      frameWidth: 34,
      frameHeight: 34
    });

    //Set initial data
    this.registry.set('playerData', this.playerStats);
    this.registry.set('locationData', this.locations);
  },

  create: function() {
    // start the WorldScene
    this.scene.start('WorldScene');
    this.scene.start('StatusBarScene');
  }
});

var StatusBarScene = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize: function StatusBarScene() {
    Phaser.Scene.call(this, { key: 'StatusBarScene' });
    this.x = 1;
    this.y = 189;
  },
  create: function() {
    // draw some background for the menu
    this.statusBar = this.add.graphics();
    this.statusBar.lineStyle(1, 0x222222);
    this.statusBar.fillStyle(0xcdcdcd, 1);
    this.statusBar.strokeRect(this.x, this.y, 318, 50);
    this.statusBar.fillRect(this.x, this.y, 318, 50);

    //make subBar container
    this.playerImg = this.statusBar.strokeRect(this.x + 85, this.y + 5, 25, 25);
    this.dragon1 = this.statusBar.strokeRect(this.x + 115, this.y + 5, 25, 25);
    this.dragon2 = this.statusBar.strokeRect(this.x + 145, this.y + 5, 25, 25);
    this.dragon3 = this.statusBar.strokeRect(this.x + 175, this.y + 5, 25, 25);
    this.playerLevel = this.add.text(this.x + 5, this.y + 5, 'Dragoon Level: 1', {
      fontSize: '12',
      color: '#000000'
    });

    this.menuButton = this.add
      .text(this.x + 290, this.y + 5, 'Menu', { fontSize: '12', color: '#000000' })
      .setInteractive()
      .on('pointerdown', () => {
        this.scene.launch('MenuScene');
      });

    this.statusBarContainer = this.add.container();
    this.statusBarContainer.add(this.playerImg);
    this.statusBarContainer.add(this.dragon1);
    this.statusBarContainer.add(this.playerLevel);
    this.statusBarContainer.add(this.menuButton);
  }
});

var WorldScene = new Phaser.Class({
  Extends: Phaser.Scene,

  initialize: function WorldScene() {
    Phaser.Scene.call(this, { key: 'WorldScene' });
  },

  preload: function() {},

  create: function() {
    // create the map
    var map = this.make.tilemap({ key: 'map' });

    // first parameter is the name of the tilemap in tiled
    var tiles = map.addTilesetImage('town1', 'tiles');

    // creating the layers
    var base = map.createStaticLayer('base', tiles, 0, 0);
    var obstacles = map.createStaticLayer('top', tiles, 0, 0);

    //build status bar
    this.sys.events.on('wake', this.buildStatusBar, this);

    // make all tiles in obstacles collidable
    obstacles.setCollisionByExclusion([-1]);

    //  animation with key 'left', we don't need left and right as we will use one and flip the sprite
    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('player', { frames: [1, 7, 1, 13] }),
      frameRate: 10,
      repeat: -1
    });

    // animation with key 'right'
    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('player', { frames: [1, 7, 1, 13] }),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'up',
      frames: this.anims.generateFrameNumbers('player', { frames: [2, 8, 2, 14] }),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: 'down',
      frames: this.anims.generateFrameNumbers('player', { frames: [0, 6, 0, 12] }),
      frameRate: 10,
      repeat: -1
    });

    // our player sprite created through the phycis system
    this.player = this.physics.add.sprite(200, 200, 'player', 6);

    // don't go out of the map
    this.physics.world.bounds.width = map.widthInPixels;
    this.physics.world.bounds.height = map.heightInPixels;
    this.player.setCollideWorldBounds(true);

    // don't walk on trees
    this.physics.add.collider(this.player, obstacles);

    // limit camera to map
    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.startFollow(this.player);
    this.cameras.main.roundPixels = true; // avoid tile bleed

    // user input
    this.cursors = this.input.keyboard.createCursorKeys();

    // where the enemies will be
    this.spawns = this.physics.add.group({ classType: Phaser.GameObjects.Zone });
    for (var i = 0; i < 30; i++) {
      var x = Phaser.Math.RND.between(0, this.physics.world.bounds.width);
      var y = Phaser.Math.RND.between(0, this.physics.world.bounds.height);
      // parameters are x, y, width, height
      this.spawns.create(x, y, 20, 20);
    }

    //entrances
    map.objects[0].objects.map(entrance => {
      this[entrance.name] = this.add.image(entrance.x + 10, entrance.y + 10, 'map');
      this.physics.add.existing(this[entrance.name], 1);
      this.physics.add.collider(
        this.player,
        this[entrance.name],
        () => this.goIndoors(entrance.id),
        false,
        this
      );
    });

    // add collider
    this.physics.add.overlap(this.player, this.spawns, this.onMeetEnemy, false, this);
  },
  wake: function() {
    this.cursors.left.reset();
    this.cursors.right.reset();
    this.cursors.up.reset();
    this.cursors.down.reset();
  },
  buildStatusBar: function() {
    this.scene.wake('StatusBarScene');
  },
  onMeetEnemy: function(player, zone) {
    // we move the zone to some other location
    zone.x = Phaser.Math.RND.between(0, this.physics.world.bounds.width);
    zone.y = Phaser.Math.RND.between(0, this.physics.world.bounds.height);

    // shake the world
    this.cameras.main.shake(300);
    this.input.stopPropagation();
    // start battle
    //this.scene.switch('BattleScene');
  },
  goIndoors: function(id) {
    this.cameras.main.flash(300);
    this.scene.launch('IndoorsScene', { indoorID: id, player: this.player });
    this.scene.sleep('WorldScene');
    this.scene.sleep('StatusBarScene');
  },
  update: function(time, delta) {
    //    this.controls.update(delta);
    this.player.body.setVelocity(0);

    // Horizontal movement
    if (this.cursors.left.isDown) {
      this.player.body.setVelocityX(-80);
    } else if (this.cursors.right.isDown) {
      this.player.body.setVelocityX(80);
    }

    // Vertical movement
    if (this.cursors.up.isDown) {
      this.player.body.setVelocityY(-80);
    } else if (this.cursors.down.isDown) {
      this.player.body.setVelocityY(80);
    }

    // Update the animation last and give left/right animations precedence over up/down animations
    if (this.cursors.left.isDown) {
      this.player.anims.play('left', true);
      this.player.flipX = true;
    } else if (this.cursors.right.isDown) {
      this.player.anims.play('right', true);
      this.player.flipX = false;
    } else if (this.cursors.up.isDown) {
      this.player.anims.play('up', true);
    } else if (this.cursors.down.isDown) {
      this.player.anims.play('down', true);
    } else {
      this.player.anims.stop();
    }
  }
});
