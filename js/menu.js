var MenuScene = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize: function MenuScene() {
        Phaser.Scene.call(this, { key: "MenuScene" });
    },
    preload: function() {

    },
    create: function() {
        this.cameras.main.setBackgroundColor("rgba(0, 0, 200, 0.5)");
        this.exitButton = this.add.text(10, 10, "Exit", { fontSize: '12', color: "#000000" }).setInteractive().on('pointerdown', () => {
            this.scene.sleep("MenuScene");
        })
        this.buildMenu();
    },
    buildMenu: function() {
        this.menuContainer = this.add.container();
        this.playerMenuContainer = this.add.container();
        this.dragonMenuContainer = this.add.container();

        this.menuContainerGraphics = this.add.graphics();
        this.menuContainerGraphics.lineStyle(1, 0x222222);
        this.menuContainerGraphics.fillStyle(0xcdcdcd, 1);

        var menuFontHeader = { fontSize: '14px', fill: '#000' }

        //Player Menu
        this.menuContainerGraphics.strokeRect(10, 20, 145, 150);
        this.menuContainerGraphics.fillRect(10, 20, 145, 150);
        this.playerHeader = this.add.text(15, 25, "Player", menuFontHeader);

        const { armour } = this.registry.list.playerData;

        //Build armour
        armour.map((item, i) => {
            new Item(this, 30, (i * 34) + 60, 34, 34, "item", [item.id]).setScale(.7);
        })



        //Dragon Menu
        this.menuContainerGraphics.strokeRect(165, 20, 145, 150);
        this.menuContainerGraphics.fillRect(165, 20, 145, 150);
        this.dragonHeader = this.add.text(170, 25, "Dragons", menuFontHeader)





        //Build Menus
        this.menuContainer.add(this.playerMenuContainer);
        this.menuContainer.add(this.dragonMenuContainer);
    }

})